import { slug } from 'github-slugger'
import { allCoreContent } from 'pliny/utils/contentlayer'
import siteMetadata from '../../../../data/siteMetadata'
import ListLayout from '@/layouts/ListLayoutWithTags'
import { allBlogs } from 'contentlayer/generated'
import tagData from '@/tag-data.json'
import { genPageMetadata } from '@/app/seo'
import { Metadata } from 'next'
import filter from 'lodash/filter'
import map from 'lodash/map'

export async function generateMetadata({ params }: { params: { tag: string } }): Promise<Metadata> {
  const tag = params.tag
  return genPageMetadata({
    title: tag,
    description: `${siteMetadata.title} ${tag} tagged content`,
    alternates: {
      canonical: './',
      types: {
        'application/rss+xml': `${siteMetadata.siteUrl}/tags/${tag}/feed.xml`,
      },
    },
  })
}

export const generateStaticParams = async () => {
  const tagCounts = tagData as Record<string, number>
  const tagKeys = Object.keys(tagCounts)
  const paths = tagKeys.map((tag) => ({
    tag: tag,
  }))
  return paths
}

export default function TagPage({ params }: { params: { tag: string } }) {
  const { tag } = params
  const title = `${tag[0].toUpperCase()}${tag.split(' ').join('-').slice(1)}`
  const filteredPosts = allCoreContent(
    filter(allBlogs, 
      (post) => post.draft !== true && post.tags && map(post.tags, (t) => slug(t)).includes(tag)
    )
  )
  return <ListLayout posts={filteredPosts} title={title} />
}
